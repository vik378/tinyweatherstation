# TinyWeatherStation

A sample Capella project that defines a TinyWeatherStation - a quick to build playground for getting hands on technologies like RaspberryPi, MQTT, Grafana, as well as discovering IoT painpoints in practice.

# Getting started

To open the model you will need Capella 5.2


# License
Licensed under Apache 2.0 License, see LICENSE file for details.
